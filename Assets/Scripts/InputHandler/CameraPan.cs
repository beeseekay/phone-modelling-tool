﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class CameraPan : MonoBehaviour {

	public MainMenuBtn mMainMenu;
	public float panSpeed = 100.0f;
	public float twistSpeed = 2.0f;
	public float dragDeadzone = 0.5f;
	public float twistDeadzone = 0.05f;
	public float twistDragMax = 5.0f;

	private Rigidbody rb;

    void Start () {
		rb = GetComponent<Rigidbody>();
    }

	void Update () {
		//Vector transforms relative to camera in case we end up rotating it around for any reason in the future, but we probably wont.
		//One finger drag adds torque in direction relative to camera position
		if (Lean.LeanTouch.Fingers.Count == 1 && Lean.LeanTouch.DragDelta.magnitude > dragDeadzone && mMainMenu.active == false) {
			Vector2 dragDirection = Lean.LeanTouch.DragDelta;
			rb.AddTorque (Camera.main.transform.up * -dragDirection.x);
			rb.AddTorque (Camera.main.transform.right * dragDirection.y);
		}

		//Two finger drag adds force to x and y of rigidbody. Moves watch around screen.
		//Magnitude of force approaches 0 the closer to the edges to direct user to keep it centered and stop them from loosing it by panning to far.
		if (Lean.LeanTouch.Fingers.Count == 2 && Mathf.Abs(Lean.LeanTouch.TwistDegrees) > twistDeadzone && Lean.LeanTouch.DragDelta.magnitude < twistDragMax) {
			rb.AddTorque (Camera.main.transform.forward * Lean.LeanTouch.TwistDegrees * twistSpeed);
		} 

		//Twist for sideways rotation. You should get how it works by now. 
		//Wont work if trying to rotate instead in case the users bad with two finger controls.
		else if (Lean.LeanTouch.Fingers.Count == 2 && Lean.LeanTouch.DragDelta.magnitude > dragDeadzone && mMainMenu.active == false) {
			rb.AddForce (panSpeed * Lean.LeanTouch.DragDelta);
		}
			
		//Pinch/pull for zooming in and out. 
		//if (Lean.LeanTouch.PinchScale > 1.001  && Lean.LeanTouch.PinchScale < 0.999) {
			//float pinchScale = Lean.LeanTouch.PinchScale * 3;
			//Debug.Log (pinchScale);
			//transform.localScale * 3.0;
		//}
	}
}