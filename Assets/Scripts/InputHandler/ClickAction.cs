﻿using UnityEngine;
using System.Collections;
using System;
using AssemblyCSharp;

public class ClickAction : MonoBehaviour {
	Ray ray;

	// Component selection
	Renderer selectedComponentRenderer;
	Renderer lastComponentRenderer;

	public string selectedComponentTag;

	GameObject selectedComponentObject = null;
	GameObject lastComponentObject = null;

	// Indicator selection
	IndicatorBehaviour selectedIndicator = null;
	IndicatorBehaviour lastIndicator = null;

	public GameObject colorPanel;
	public GameObject mWatchBezel;
    public Material mGlassMaterial;
    public Material mOriginalMaterial;
    public Material mSavedMaterial;

	private bool scaling = false;

	void Start () {
		Lean.LeanTouch.OnFingerTap += OnFingerTap;
		Lean.LeanTouch.OnPinch += OnPinch;
		Lean.LeanTouch.OnFingerHeldSet  += OnFingerHeld;
		Lean.LeanTouch.OnFingerUp       += OnFingerUp;
	}
	// Update is called once per frame
	void Update () {
		
	}
	public void OnFingerHeld(Lean.LeanFinger finger)
	{
		if (finger.Age > 1 && finger.TotalDeltaMagnitude < 10)
			LongPress ();
	}

	private void OnFingerTap(Lean.LeanFinger finger) {
		var ray = finger.GetRay();
		var hit = default(RaycastHit);

		bool didHit = Physics.Raycast (ray, out hit, 1500.0f);
		if (didHit) {
			lastIndicator = selectedIndicator;
			if (hit.collider.tag == "Indicator") {
				if (lastIndicator != null)
					lastIndicator.Unselected ();
				selectedIndicator = hit.collider.gameObject.GetComponent<IndicatorBehaviour> ();
				selectedIndicator.Selected ();
			}
		} else {
		}
	}

	private void OnPinch(float scale) {
		if (selectedComponentObject != null) {
			selectedComponentObject.GetComponent<WatchBehaviour> ().UpdateDimension (scale);
			scaling = true;
		}
	}

	public void OnFingerUp(Lean.LeanFinger finger)
	{
		if (scaling && selectedComponentObject != null) {
			scaling = false;
			selectedComponentObject.GetComponent<WatchBehaviour> ().UpdateIndicators ();
		}
	}

	private void LongPress() {
		Ray toMouse = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit rhInfo;
		//Raycast return true or false
		bool didHit = Physics.Raycast(toMouse, out rhInfo, 1500.0f);
		if(didHit) {
//			Debug.Log(rhInfo.collider.name + "  " + rhInfo.point + "Tag: " + rhInfo.collider.tag + "Object: " + rhInfo.collider.gameObject);
//			Debug.DrawRay(ray.origin, ray.direction * 20, Color.red);
			lastComponentRenderer = selectedComponentRenderer;
			lastComponentObject = selectedComponentObject;

			if(rhInfo.collider.tag == "WatchLug") {
				colorPanel.SetActive(true);

				AssignSelected (rhInfo);

			}

			else if(rhInfo.collider.tag == "WatchCase") {
				colorPanel.SetActive(true);

				AssignSelected (rhInfo);
				selectedComponentObject.GetComponent<WatchBehaviour> ().Active = true;
			}

			else if(rhInfo.collider.tag == "WatchBezel") {
				colorPanel.SetActive(true);

				AssignSelected (rhInfo);
				selectedComponentObject.GetComponent<WatchBehaviour> ().Active = true;
			}
//			Debug.Log(rhInfo.collider.name + "  ");
		} else {
//			Debug.Log("Not hit");

			lastComponentRenderer = selectedComponentRenderer;
			lastComponentObject = selectedComponentObject;
			if (lastComponentRenderer != null)
				lastComponentRenderer.sharedMaterial = mOriginalMaterial;
//				lastComponentRenderer.material.color = Color.white;
			if (lastComponentObject != null) {
				lastComponentObject.GetComponent<WatchBehaviour> ().Active = false;
			}
			selectedComponentRenderer = null;
			selectedComponentObject = null;
		}
	}

	private void HighlightComponent() {
        // Save original material before selected
        Debug.Log(selectedComponentRenderer);

        // When selected new component
		if (selectedComponentRenderer != lastComponentRenderer) {
			// Save Material in the temporary material
            mSavedMaterial = selectedComponentRenderer.sharedMaterial;

			// Change the material to glass when it selected
            selectedComponentRenderer.sharedMaterial = mGlassMaterial;
            //selectedComponentRenderer.material.shader = Shader.Find ("Standard");

            if (lastComponentRenderer != null) {
                // Change back to original material when other components selected
                lastComponentRenderer.sharedMaterial = mOriginalMaterial;
				//lastComponentRenderer.material.shader = Shader.Find ("Standard");
        	}
        }
 	}

	private void AssignSelected(RaycastHit ray) {
		selectedComponentObject = ray.collider.gameObject;
		if (lastComponentObject == null || lastComponentObject != selectedComponentObject) {
			selectedComponentRenderer = ray.collider.GetComponent<Renderer> ();

			selectedComponentObject.GetComponent<WatchBehaviour> ().Active = true;

			if (lastComponentObject != null) {
				lastComponentObject.GetComponent<WatchBehaviour> ().Active = false;
			}

			selectedComponentTag = ray.collider.tag;
			Debug.Log ("assigned" + selectedComponentTag);
			HighlightComponent ();
		}
	}

	// Return selected renderer
	public Renderer getRenderer() {
		return selectedComponentRenderer;
	}

}
