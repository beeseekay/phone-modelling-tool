﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class WatchMove : MonoBehaviour {

	public MainMenuBtn mMainMenu;

	public float rotateSpeed = 1.0f;
	public float panSpeed = 5.0f;
	public float dragDeadzone = 1.0f;

	public float twistSpeed = 2.0f;
	public float twistDeadzone = 0.05f;
	public float twistDragMax = 5.0f;

	public float pinchDeadzone = 0.005f;
	public float zoomMin = 0.8f;
	public float zoomMax = 3f;

	private Rigidbody rb;

    void Start () {
		rb = GetComponent<Rigidbody>();
    }

	void Update () {
		//Vector transforms relative to camera in case we end up rotating it around for any reason in the future, but we probably wont.
		//One finger drag adds torque in direction relative to camera position
		//Checks for correct fingers, moving faster than deadzone (in case trying to hold down), menu active (switch with viewMode in future), and also   
		if (Lean.LeanTouch.Fingers.Count == 1 && Lean.LeanTouch.DragDelta.magnitude > dragDeadzone && mMainMenu.active == false && Lean.LeanTouch.Fingers[0].SnapshotDuration > 0.1) {
			Vector2 dragDirection = Lean.LeanTouch.DragDelta;
			rb.AddTorque (Camera.main.transform.up * -dragDirection.x * rotateSpeed);
			rb.AddTorque (Camera.main.transform.right * dragDirection.y * rotateSpeed);
		}

		//Two finger drag adds force to x and y of rigidbody. Moves watch around screen.
		//Magnitude of force approaches 0 the closer to the edges to direct user to keep it centered and stop them from loosing it by panning to far.
		if (Lean.LeanTouch.Fingers.Count == 2 && Mathf.Abs(Lean.LeanTouch.TwistDegrees) > twistDeadzone && Lean.LeanTouch.DragDelta.magnitude < twistDragMax) {
			rb.AddTorque (Camera.main.transform.forward * Lean.LeanTouch.TwistDegrees * twistSpeed);
		} 

		//Twist for sideways rotation. You should get how it works by now. 
		//Wont work if trying to rotate instead in case the users bad with two finger controls.
		else if (Lean.LeanTouch.Fingers.Count == 2 && Lean.LeanTouch.DragDelta.magnitude > dragDeadzone && mMainMenu.active == false) {
			rb.AddForce (panSpeed * Lean.LeanTouch.DragDelta);
		}
			
		//Pinch/pull for zooming in and out. 
		if (((Lean.LeanTouch.PinchScale > 1 + pinchDeadzone && transform.localScale.x < zoomMax)  || (Lean.LeanTouch.PinchScale < 1 - pinchDeadzone && transform.localScale.x > zoomMin)) && Lean.LeanTouch.DragDelta.magnitude < dragDeadzone) {
			float pinchScale = Lean.LeanTouch.PinchScale;
			transform.localScale *= pinchScale;
		}
	}
}