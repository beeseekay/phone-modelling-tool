﻿using UnityEngine;
using System.Collections;

public class MainMenuBtn : MonoBehaviour {

	public GameObject GreyScreen;
	public GameObject ColorWheel;
	public bool active = false;
	public Animation menuAnim, greyScreenAnim;

	//Turned enable and disable into one toggle function
	public void ToggleMenu() {
		//Set all states in respect to active bool.
		active = !active;
		//Play animations in forward or reverse if 
		if (active) {
			menuAnim.Play ("Menu Open");
			greyScreenAnim.Play ("Fade In");
		} else {
			menuAnim.Play("Menu Close");
			greyScreenAnim.Play("Fade Out");
		}
		//Disable colour panel when menu is on
		//mColorPanel.SetActive(!active);
		// Turn on gray screen to cover watch model
		// mGrayScreen.SetActive(true);
	}

	// Disable Main Panel 06.09.16 Dongyeop
//	public void DisableBoolAnimator (Animator anim) {
//		active = false;
//		anim.Play ("SlideOut");
//		mMainMenuClose.SetActive(false);
//		mMainMenuOpen.SetActive(true);
//		mWatch.SetActive(true);
//		mGrayScreen.SetActive(false);
//		mColorPanel.SetActive(true);
		//mTouchScript.SetActive(true);

			//spriteRenderer.sprite = mainMenuOff;
		
//	}
}
