﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComponentSelect : MonoBehaviour {

	public Rigidbody watchRb;
	public GameObject selectedComponent;

	protected virtual void OnEnable() {
		Lean.LeanTouch.OnFingerTap += OnFingerTap;
	}

	private void OnFingerTap(Lean.LeanFinger finger) {
		Debug.Log (finger.ScreenPosition);
		//Disable and reenable rigidbody so collider 
		watchRb.isKinematic = true;
		RaycastHit hitInfo = new RaycastHit();
		if (Physics.Raycast (Camera.main.ScreenPointToRay (finger.ScreenPosition), out hitInfo) && hitInfo.collider.tag == "Editable Component") {
			Debug.Log(hitInfo.collider.name);
			selectedComponent = hitInfo.collider.gameObject;
		}
		watchRb.isKinematic = false;

	}
}