﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;
using ProceduralToolkit;

public class WatchBezel : WatchBehaviour {
	public WatchCase caseScript;
	public WatchGlass glass;

	private float bottomOutRadius = .525f;
	public float BottomOutRadius { 
		get { return bottomOutRadius; }
		set { bottomOutRadius = value; }
	}
	private float bottomInRadius = .425f;
	public float BottomInRadius {
		get { return bottomInRadius; }
		set { bottomInRadius = value; }
	}
	private float topOutRadius = .440f;
	public float TopOutRadius { 
		get { return topOutRadius; }
		set { topOutRadius = value; }
	}
	private float topInRadius = .425f;
	public float TopInRadius { 
		get { return topInRadius; }
		set { topInRadius = value; }
	}
	private float height = 0.05f;
	public float Height { 
		get { return height; }
		set { height = value; }
	}

	public int nbSides { get; set; }

	// Use this for initialization
	void Start () {
		filter = GetComponent<MeshFilter> ();

		indicatorHandlers = GetComponentsInChildren<IndicatorBehaviour> ();

		nbSides = 60;

		Active = false;
		Generate ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Active) {
			foreach (IndicatorBehaviour ib in indicatorHandlers)
				ib.Active ();
		} else {
			foreach (IndicatorBehaviour ib in indicatorHandlers)
				ib.Inactive ();
		}
	}

	public void Generate() {
		MeshDraft draft = MeshDraft.WatchCase2 (height, nbSides, bottomOutRadius, bottomInRadius, topOutRadius, topInRadius);
		draft = MeshDraft.OffSet (draft, new Vector3 (0, caseScript.Height / 2 + height / 2, 0));

		filter.mesh = draft.ToMesh ();
		filter.gameObject.AddComponent<MeshCollider> ();
	}

	public override void UpdateDimension (float scale) {
		scaling = true;
		WatchDimension target = WatchDimension.NONE;
		foreach (IndicatorBehaviour ib in indicatorHandlers) {
			if (ib.isSelected ())
				target = ib.target;
		}

		switch (target) {
		case WatchDimension.HEIGHT:
			UpdateHeight (scale);
			break;
		case WatchDimension.OUTRADIUS:
			UpdateRadius (scale);
			break;
		default:
			break;
		}
	}

	public void UpdateHeight(float scale) {
		height *= scale;
		if (height <= .001f)
			height = .001f;
		if (height >= .1f)
			height = .1f;
		ReflectUpdate ();
	}

	public void UpdateRadius(float scale) {
		scale = (scale - 1) / 10 + 1;
		bottomOutRadius *= scale;
		if (bottomOutRadius <= 0.5f)
			bottomOutRadius = 0.5f;
		if (bottomOutRadius >= caseScript.OutRadius)
			bottomOutRadius = caseScript.OutRadius;
		ReflectUpdate ();
	}

	public override void ReflectUpdate() {
		Dispose ();

		Generate ();
		glass.ReflectUpdate ();
	}
}
