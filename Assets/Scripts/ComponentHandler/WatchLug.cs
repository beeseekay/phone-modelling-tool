﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;
using ProceduralToolkit;

public class WatchLug : WatchBehaviour {
	public WatchCase caseScript;

	public float height { get; set; }
	public float topWidth { get; set; }
	public float bottomWidth { get; set; }
	public float sideWidth { get; set; }

	// Use this for initialization
	void Start () {
		filter = GetComponent<MeshFilter> ();
		Generate ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Generate() {
		MeshDraft draft = InitLugs ();

		filter.mesh = draft.ToMesh ();
		filter.gameObject.AddComponent<MeshCollider> ();
	}

	private MeshDraft InitLugs() {
		Vector3 right = Vector3.right * (caseScript.OutRadius/2);
		Vector3 forward = Vector3.forward * (caseScript.OutRadius - 0.025f);

		var legCenters = new Vector3[]
		{
			-right - forward,
			right - forward,
			right + forward,
			-right + forward
		};

		// Add 4 lugs in the MeshFilter
		var draftLugs = LugOffset(legCenters[0], 0.25f, 0.04f);
		draftLugs.Add(LugOffset(legCenters[1], 0.25f, 0.04f));
		draftLugs.Add(LugOffset(legCenters[2], 0.25f, 0.04f));
		draftLugs.Add(LugOffset(legCenters[3], 0.25f, 0.04f));
		return draftLugs;
	}

	private MeshDraft LugOffset(Vector3 center, float width, float height)
	{
		var draft = MeshDraft.Hexahedron(0.15f, width, height);
		//Move the positions of the lugs
		draft.Move(center);
		return draft;
	}

	public override void ReflectUpdate() {
		Dispose ();
		Generate ();
	}
}
