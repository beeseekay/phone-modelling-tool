﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using AssemblyCSharp;
using ProceduralToolkit.Examples;
using ProceduralToolkit;

public class WatchCase : WatchBehaviour {
	
	public WatchBezel bezel;
	public WatchLug lug;

	private float height = 0.1f;
	public float Height { 
		get { return height; }
		set { height = value; }
	}
	private float outRadius = .575f;
	public float OutRadius { 
		get { return outRadius; }
		set { outRadius = value; }
	}
	private float inRadius = .425f;
	public float InRadius { 
		get { return inRadius; }
		set { inRadius = value; }
	}
	public int nbSides { get; set; }
	// Use this for initialization
	void Start () {
		filter = GetComponent<MeshFilter> ();

		indicatorHandlers = GetComponentsInChildren<IndicatorBehaviour> ();

		nbSides = 60;

		Active = false;
		Generate ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Active) {
			foreach (IndicatorBehaviour ib in indicatorHandlers)
				ib.Active ();
		} else {
			foreach (IndicatorBehaviour ib in indicatorHandlers)
				ib.Inactive ();
		}
	}

	public void Generate() {
		MeshDraft draft = MeshDraft.WatchCase2 (height, nbSides, outRadius, inRadius, outRadius, inRadius);

		filter.mesh = draft.ToMesh ();
		filter.gameObject.AddComponent<MeshCollider> ();
	}


	public override void UpdateDimension (float scale) {
		scaling = true;
		WatchDimension target = WatchDimension.NONE;
		foreach (IndicatorBehaviour ib in indicatorHandlers) {
			if (ib.isSelected ())
				target = ib.target;
		}

		switch (target) {
		case WatchDimension.HEIGHT:
			UpdateHeight (scale);
			break;
		case WatchDimension.OUTRADIUS:
			UpdateRadius (scale);
			break;
		default:
			break;
		}

	}

	public void UpdateHeight(float scale) {
		height *= scale;
		if (height <= 0.1f)
			height = 0.1f;
		if (height >= .2f)
			height = .2f;
		ReflectUpdate ();
	}

	public void UpdateRadius(float scale) {
		scale = (scale - 1) / 10 + 1;
		outRadius *= scale;
		if (outRadius <= bezel.BottomOutRadius)
			outRadius = bezel.BottomOutRadius;
		if (outRadius >= .7f)
			outRadius = .7f;
		ReflectUpdate ();
	}

	public override void ReflectUpdate() {
		Dispose ();
		Generate ();
		bezel.ReflectUpdate ();
		lug.ReflectUpdate ();
	}
}
