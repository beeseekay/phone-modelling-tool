﻿using System;

namespace AssemblyCSharp
{
	public enum WatchComponent
	{
		CASE,
		BEZEL,
		LUG,
		GLASS
	}
}

