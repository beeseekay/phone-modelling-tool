﻿using System;
using UnityEngine;

namespace AssemblyCSharp
{
	public class IndicatorBehaviour: MonoBehaviour
	{
		public GameObject center;
		protected MeshRenderer render;
		protected MeshFilter filter;
		protected CameraControl viewDetector;
		protected Indicator indicator;

		protected Color selectedColor = Color.blue;
		protected Color unselectedColor = Color.gray;

		protected bool active = false;
		protected bool selected = false;

		public WatchDimension target;

		public void Selected() {
			render.material.color = selectedColor;
			selected = true;
		}

		public void Unselected() {
			render.material.color = unselectedColor;
			selected = false;
		}

		public void Active() {
			active = true;
		}

		public void Inactive() {
			active = false;
		}

		public bool isSelected() {
			return selected;
		}
			
		public void init() {
			filter.mesh = indicator.ToMesh ();
			render.material.color = selected ? selectedColor : unselectedColor;
			filter.gameObject.AddComponent<MeshCollider> ();
		}

		public virtual void Generate() {
			Debug.Log ("Generate not Implemented yet");
		}

		public void Dispose() {
			filter.mesh.Clear ();
			Destroy (filter.GetComponent<MeshCollider> ());
		}

		public void ReflectUpdate() {
			
			Dispose ();
			Generate ();
			init ();
		}
	}
}

