﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityExtension;
using Random = UnityEngine.Random;

public class OBJ_Generator : MonoBehaviour {

	// This Path for to save OBJ files
	//------------------------------------------------------------------------------------------------------------	
		private const string INPUT_PATH = @"Assets/Cylinder_x.obj"; //Ignore, we don't have input yet.
		private const string OUTPUT_PATH_CASE = @"Assets/Export/Watch_Case.obj";
		private const string OUTPUT_PATH_BEZEL = @"Assets/Export/Watch_Bezel.obj";
		private const string OUTPUT_PATH_LUG = @"Assets/Export/Watch_Lug.obj";
	//------------------------------------------------------------------------------------------------------------	
    // Mesh Fileter to draw

	public MeshFilter meshFilterCase;
	public MeshFilter meshFilterLug;
    public MeshFilter meshFilterBezel;
    public MeshFilter meshFilterGlass;

	/*
    13.09.16 Dongyeop
    This function is to genarate OBJ from runtime 3D object.
    Each parts will be indivisual OBJ file in the Asset/Export folder.
    */
    public void generateOBJ()
    {
		// To export as an OBJ
		var vStreamCase = new FileStream(OUTPUT_PATH_CASE, FileMode.Create);
		var vStreamBezel = new FileStream(OUTPUT_PATH_BEZEL, FileMode.Create);
		var vStreamLug = new FileStream(OUTPUT_PATH_LUG, FileMode.Create);

		// Encode mesh to OBJ
		var vOBJDataCase = meshFilterCase.mesh.EncodeOBJ();
		var vOBJDataBezel = meshFilterBezel.mesh.EncodeOBJ();
		var vOBJDataLug = meshFilterLug.mesh.EncodeOBJ();

		//Create OBJ files
		OBJLoader.ExportOBJ(vOBJDataCase, vStreamCase);
		OBJLoader.ExportOBJ(vOBJDataBezel, vStreamBezel);
		OBJLoader.ExportOBJ(vOBJDataLug, vStreamLug);

		// Close the stream
		vStreamCase.Close();
		//vStreamBezel.Close();
		//vStreamLug.Close();
    }
}
