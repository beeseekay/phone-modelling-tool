﻿using UnityEngine;
using System.Collections;

public class SceneManager : MonoBehaviour {

	public void LoadVRLevel() {
		Application.LoadLevel ("WatchVR");
	}

	public void LoadEditMode() {
		Application.LoadLevel ("WatchGeneratorNexus");
	}

	public void LoadSelectMode() {
		Application.LoadLevel ("WatchSelectorNexus");
	}
}
