﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;

public class FacebookManager : MonoBehaviour {

	private static FacebookManager _instance;

	public static FacebookManager Instance
	{
		get {
			if (_instance == null) {
				GameObject fbm = new GameObject("FBManager");;
				fbm.AddComponent<FacebookManager> ();
			}

			return _instance;
		}
	}

	public bool IsLoggedIn { get; set; }
	public string ProfileName { get; set; }
	public string ProfileEmail { get; set; }
	public Sprite ProfilePic { get; set; }
	public string AppLinkURL { get; set; }

	void Awake()
	{
		//Make sure keep between the scene
		DontDestroyOnLoad (this.gameObject);

		_instance = this;

		IsLoggedIn = true;
	}

	public void InitFB()
	{
		if (!FB.IsInitialized) {
			FB.Init(SetInit, OnHideUnity);
		} else {
			IsLoggedIn = FB.IsLoggedIn;
		}
	}

	void SetInit() {
		if (FB.IsLoggedIn) {
			Debug.Log("FB is logged in");
			//Get Profile
			GetProfile();
		} else {
			Debug.Log("FB is not logged in");
		}
		IsLoggedIn = FB.IsLoggedIn;
	}

	void OnHideUnity(bool isGameShown) {
		if (!isGameShown) {
			Time.timeScale = 0;
		} else {
			Time.timeScale = 1;
		}
	}

	public void GetProfile()
	{
		// Get the username, grap the name
		FB.API("/me?fileds=first_name", HttpMethod.GET, DisplayUsername);
		FB.API ("/me/picture?type=square&height=128&width=128", HttpMethod.GET, DisplayProfilePic);
		FB.GetAppLink(DealWithAppLink);
	}

	//Display the username
	void DisplayUsername(IResult result)
    {

        //Text UserName = DialogUserName.GetComponent<Text> ();

        if (result.Error == null) {
            ProfileName = "" + result.ResultDictionary ["name"].ToString();
        } else {
            Debug.Log (result.Error);
        }

    }

	//Display the email
	void DisplayEmail(IResult result)
    {

        //Text UserName = DialogUserName.GetComponent<Text> ();

        if (result.Error == null) {
            ProfileName = "" + result.ResultDictionary ["email"].ToString();
        } else {
            Debug.Log (result.Error);
        }

    }

	void DisplayProfilePic(IGraphResult result)
    {
        if (result.Texture != null) {
            // Cast texture as a spirte to use in Unity
            ProfilePic = Sprite.Create (result.Texture, new Rect (0, 0, 128, 128), new Vector2 ());
        }
    }

    void DealWithAppLink(IAppLinkResult result)
    {
    	if(!String.IsNullOrEmpty(result.Url)) {
    		AppLinkURL = result.Url;
    	}
    }

    public void Share()
    {
    	FB.FeedShare (
    		string.Empty, // Default user
    		new Uri(AppLinkURL),
    		"Hellow this is the title",
    		"This is the caption",
    		"Check out this watch",
    		new Uri("http://1159watch.pe.hu/daytona.jpg"), // Image to add
    		string.Empty, // media, audio or video
    		ShareCallback
    	);
    }

    void ShareCallback(IResult result)
    {
    	if (result.Cancelled) {
    		Debug.Log ("Share Cancelled");
    	} else if (!string.IsNullOrEmpty (result.Error)) {
    		Debug.Log ("Error on share!");
		} else if (!string.IsNullOrEmpty (result.RawResult)) {
			Debug.Log ("Success Sharing!");
    	}
    }

    public void Invite()
    {
    	FB.Mobile.AppInvite (
    		new Uri(AppLinkURL),
			new Uri("http://1159watch.pe.hu/daytona.jpg"),
			InviteCallback
    	);
    }

    void InviteCallback(IResult result)
	{
    	if (result.Cancelled) {
    		Debug.Log ("Share Cancelled");
    	} else if (!string.IsNullOrEmpty (result.Error)) {
    		Debug.Log ("Error on invite!");
		} else if (!string.IsNullOrEmpty (result.RawResult)) {
			Debug.Log ("Success Invite!");
    	}
    }
}
